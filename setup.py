from setuptools import setup, find_namespace_packages


setup(
        name='nude-man',
        version='1.0a2',
        packages=find_namespace_packages(include=[ 'nude_man.*' ]),
        entry_points={
                'console_scripts': [
                        'nude-man = nude_man.__main__:main'
                        ]
                },
        url='https://softworks.inspyre.tech/nude-man',
        license='MIT',
        author='Taylor-Jayde Blackstone',
        author_email='t.blackstone@inspyre.tech',
        description='Scans a given directory recursively for pictures that could be deemed NSFW and sweeps them into a given output directory.'
        )
