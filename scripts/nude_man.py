#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Created on Mon Jul 20 04:34:19 2020.

@author: taylor
"""
import enum
import json
import os
import shutil
import fnmatch
from pathlib import Path

import subprocess
from nude_man.lib.config import default_data_out
from datetime import datetime
from time import sleep
from logging import getLogger

import PIL.Image as Image
import requests
import tqdm
from halo import Halo
from inspy_logger import start as start_logger

import nude_man
from nude_man import DEFAULT_DATA_DIR
from nude_man.lib.config import default_data_out, default_data_in, Config
from nude_man.lib.config.args import parse
from nude_man.lib.config.environment import Environment
from nude_man.lib.helpers.debug import format_members
from nude_man.lib.helpers.files import makedir_confirm, grab_paths as grab_file_paths

# Import error classes
from nude_man.lib.errors.api import (
    APIError,
    InvalidKeyError,
    KeyMissingError,
    ScoringError,
)
from nude_man.lib.errors.app import NoFilesMatchError


def main(args):

    log_name = "NudeMan.main"

    log = getLogger(log_name)
    env = Environment(args)

    log.debug(f"{dir(env)}")

    _conf = Config(args, env)

    config = _conf.conf

    log.debug(
        f'Found a config file with the following sections: {",".join(config.sections())}'
    )

    start_dir = args.source_start

    if start_dir:
        if "~" in start_dir:
            start_dir = os.path.expanduser(start_dir)
        else:
            start_dir = str(args.source_start)
    else:
        start_dir = os.getcwd()

    log.debug(f"Starting search in {start_dir}")

    out_dir = config["SETTINGS"]["output-dir"]

    if not out_dir == args.output_directory:

        od = Path(args.output_directory).resolve()
        log.debug(
            f"User specified an output directory that differs from the one loaded in the config file: {args.output_directory}"
        )
        if not od.exists:
            makedir_confirm(od)

        out_dir = od

        log.debug(f"Output directory is set as {out_dir}")

    else:
        log.debug(
            f"No custom output directory received. Falling back on default path: {args.output_directory}"
        )
        out_dir = args.output_directory

    file_paths = grab_file_paths(start_dir, recursive=args.recursive)
    num_files = len(file_paths)
    log.debug(f"Found {num_files} files")
    matched_files = nude_man.score_list(file_paths, config)

    try:
        nude_man.conclude(args, matched_files, out_dir)
    except NoFilesMatchError as e:
        log.error(e.message)


if __name__ == "__main__":

    # Collect our command-line arguments and parse 'em
    args = parse()
    print(dir(args))
    print(args.subcommand_name)
    if not args.subcommand_name == "configure":
        print("triggered")
    else:

        import nude_man.conf.configure as nm_conf

        TA = nm_conf.NudeManConfiguration()
        TA.run()
    # except AttributeError:

    #     # Create a time-stamp
    #     ts = datetime.now()

    #     # Create our root logger device, and if verbose: announce logger start.
    #     log_name = "NudeMan"
    #     log = start_logger(log_name, args.verbose)
    #     m_debug = log.debug

    #     m_debug("Main logger started for NudeMan at {ts}")

    #     dir_res = dir()

    #     members = format_members(dir_res)
    #     m_debug(f"I contain the following members: {members}")

    #     m_debug(f"Command line arguments are: {args}")

    #     m_debug(f"Main script set-up complete, moving into operations...")

    #     try:
    #         main(args)
    #     except InvalidKeyError as e:
    #         print(e.message)
    #         raise
    #     except KeyMissingError as e:
    #         print(e.message)
    #         raise
