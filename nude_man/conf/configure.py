# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 18:52:02 2020

@author: taylor
"""

import npyscreen

# This application class serves as a wrapper for the initialization of curses
# and also manages the actual forms of the application

from pathlib import Path
from configparser import ConfigParser
import os
import sys

from nude_man.lib.config.args import parse

app_pair = "/Inspyre-Softworks/NudeMan"

wizard_res = {"SETTINGS": {}}
user_home = str(Path("~").expanduser())
cache_dir = str(Path(f"{user_home}/.cache{app_pair}/run"))
env_lockfile = str(Path(f"{cache_dir}/env.lock"))
default_in_dir = str(Path(f"{user_home}/Pictures"))
default_app_dir = str(Path(f"{user_home}{app_pair}"))
default_out_dir = str(Path(f"{default_app_dir}/data/output"))


class Args(object):
    def __init__(self, arg_parser):
        self.test = "Hi"
        self.parsed = arg_parser


class NudeManConfigurationError(Exception):
    def __init__(self):
        pass


class ExistingConfigurationError(NudeManConfigurationError):
    def __init__(self, msg=None, existing_file=None):
        super().__init__()
        _msg = f"An error of type {self.__class__.__name__} has occurred"
        if existing_file is not None:
            _msg += f"A previous configuration has been found at: {existing_file}"
        self.msg = _msg
        if msg is not None:
            self.msg += msg

        self.message = self.msg


def lock_exists():
    # _filepath = os.path.dirname(os.path.realpath(__file__))

    if Path(env_lockfile).exists():
        return True
    else:
        return False


def load_lock(location=Path(env_lockfile)):
    parser = ConfigParser()
    parser.read(location)

    return parser


def remove_lock(location=Path(env_lockfile)):
    pass


class NudeManConfiguration(npyscreen.NPSAppManaged):
    def onStart(self):
        self.registerForm("MAIN", MainForm())


# This form class defines the display that will be presented to the user.


class MainForm(npyscreen.Form):
    def create(self):
        from time import sleep
        import os
        from pathlib import Path

        instructions = "- Navigate the menu with the arrow keys.\n- Press TAB in 'file' fields to navigate directories\n"

        if lock_exists():
            title = "Previous configuration found!"
            msg = "Continuing will overwrite AND REMOVE ALL previous settings and data!!\n"
            msg += "Continue?"
            if not npyscreen.notify_yes_no(msg, title):
                lock_file = load_lock()
                try:
                    raise ExistingConfigurationError(
                        existing_file=lock_file["ENVIRONMENT"]["data_dir"]
                    )
                except ExistingConfigurationError as e:
                    print(e.message)
                    print(
                        "\nIf you're attempting to edit an existing config please start the program again with the '--edit' flag"
                    )
                    sleep(3)
                    raise
        else:
            print(f"No lock at {env_lockfile}")

            sleep(2)

        npyscreen.setTheme(npyscreen.Themes.ColorfulTheme)
        self.wStatus2 = "Hello"

        self.api_key = self.add(
            npyscreen.TitleText, name=f"API Key:", value="APIKEY", begin_entry_at=24,
        )
        self.threshold = self.add(
            npyscreen.TitleSlider,
            name=f"Score Threshold:",
            out_of=100,
            step=5,
            value=75,
            use_two_lines=False,
            begin_entry_at=24,
        )
        self.app_dir = self.add(
            npyscreen.TitleFilename,
            name="App Directory:",
            value=default_app_dir,
            use_two_lines=False,
            begin_entry_at=24,
            select_dir=True,
        )
        self.source_dir = self.add(
            npyscreen.TitleFilename,
            name="Image Directory:",
            value=default_in_dir,
            use_two_lines=False,
            begin_entry_at=24,
            select_dir=True,
        )
        self.out_dir = self.add(
            npyscreen.TitleFilenameCombo,
            name="Output Directory:",
            value=default_out_dir,
            use_two_lines=False,
            begin_entry_at=24,
            select_dir=True,
        )
        self.strip_exif = self.add(
            npyscreen.TitleSelectOne,
            name="Strip EXIF data:",
            values=["No", "Yes"],
            use_two_lines=False,
            begin_entry_at=24,
            value=0,
        )

    def afterEditing(self):
        from time import sleep

        self.parentApp.setNextForm(None)
        settings = wizard_res["SETTINGS"]

        settings["api-key"] = self.api_key.value
        settings["app-dir"] = self.app_dir.value
        settings["input-dir"] = self.source_dir.value
        settings["output-dir"] = self.out_dir.value
        settings["threshold"] = str((self.threshold.value / 100))

        strip_exif = self.strip_exif.value[0]
        if strip_exif:
            settings["strip_exif"] = "true"
        else:
            settings["strip_exif"] = "false"


def write_lock(app_dir):
    from datetime import datetime

    env = {"ENVIRONMENT": {"creation_date": datetime.now(), "data_dir": app_dir,}}

    c_parser = ConfigParser()

    c_parser.read_dict(env)

    if not Path(cache_dir).exists():
        os.makedirs(Path(cache_dir))

    with open(str(cache_dir) + "/env.lock", "w") as lock_file:
        c_parser.write(lock_file)


def write_conf(conf_dict, write_lockfile=True):
    parser = ConfigParser()
    parser.read_dict(conf_dict)
    app_dir = Path(parser["SETTINGS"]["app-dir"]).resolve()

    conf_dir_str = str(app_dir) + "/run/conf"
    conf_dir = Path(conf_dir_str).resolve()

    conf_fp_str = conf_dir_str + "/nude-man.conf"
    conf_fp = Path(conf_fp_str).resolve()

    write_lock(app_dir)

    if not app_dir.exists():
        os.makedirs(conf)

    with open(conf_fp, "w") as conf_file:
        parser.write(conf_file)


if __name__ == "__main__":
    args = parse()
    TA = NudeManConfiguration()
    TA.run()
    print(wizard_res)
    write_conf(wizard_res)
