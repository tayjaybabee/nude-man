from logging import getLogger

root_log_name = 'NudeMan.app'
DEFAULT_DATA_DIR = '~/Inspyre-Softworks/NudeMan/data'


class Application(object):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def safe_exit(self, exit_reason: str, code: int):
        se_log_name = root_log_name + '.safe_exit'
        se_log = getLogger(se_log_name)
        se_log.info('Received call for a safe exit')
        if code:
            se_log.error(f'Given Reason: {exit_reason}')
            se_log.error(
                'This exit is due to a program exception. Please see program logs')
            exit(code)
        else:
            se_log.info('Program is exiting expectedly, no errors detected.')

