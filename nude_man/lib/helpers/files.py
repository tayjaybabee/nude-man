import os
import enum
from logging import getLogger
import tqdm
from pathlib import Path
import PIL.Image as Image


# Enum for size units
class SIZE_UNIT(enum.Enum):
    BYTES = 1
    KB = 2
    MB = 3
    GB = 4


class SizeCodex(object):
    def __init__(self):
        self.B = 1
        self.KB = self.B * 1024
        self.MB = self.KB * 1024
        self.GB = self.MB * 1024


def convert_unit(size_in_bytes, unit):
    """ Convert the size from bytes to other units like KB, MB or GB"""

    if unit == SIZE_UNIT.KB:
        return size_in_bytes / 1024

    elif unit == SIZE_UNIT.MB:
        return size_in_bytes / (1024 * 1024)

    elif unit == SIZE_UNIT.GB:
        return size_in_bytes / (1024 * 1024 * 1024)

    else:
        return size_in_bytes


def get_size(file_name, size_type=SIZE_UNIT.BYTES):
    """Get file in size in given unit like KB, MB or GB."""

    size = os.path.getsize(file_name)

    return size, convert_unit(size, size_type)


def grab_paths(start_dir, recursive=True):

    log_name = "NudeMan.grab_paths"
    log = getLogger(log_name)
    debug = log.debug
    debug(f"Logger started for {log_name}")

    file_paths = []
    file_exts = [".jpg"]
    for i_ext in tqdm.tqdm(file_exts):
        for name in tqdm.tqdm(Path(start_dir).rglob(f"*{i_ext}"), leave=False):
            pic = Image.open(name)
            name = str(name)
            f_name_raw = str(name).split("/")
            f_name, f_ext = f_name_raw.pop().split(".")

            size = get_size(name)

            byte = 1

            kb = byte * 1024
            fs_kb = SIZE_UNIT.KB
            kb_size = None

            mb = kb * 1024
            fs_mb = SIZE_UNIT.MB
            mb_size = None

            gb = mb * 1024
            fs_gb = SIZE_UNIT.GB
            gb_size = None

            kb_size = get_size(name, fs_kb)[1]
            mb_size = get_size(name, fs_mb)[1]
            gb_size = get_size(name, fs_gb)[1]

            if pic.format == "JPEG":
                file_obj = {
                    "filepath": name,
                    "name": f_name,
                    "ext": f_ext,
                    "b_size": size[0],
                }
                if kb_size:
                    file_obj["kb_size"] = round(kb_size, 2)
                elif mb_size:
                    file_obj["mb_size"] = round(mb_size, 2)
                elif gb_size:
                    file_obj["gb_size"] = round(gb_size, 2)
                file_paths.append(file_obj)

            pic.close()

    return file_paths


def makedir_confirm(directory):
    global final_outpath, args

    log_name = "NudeMan.makedir_confirm"
    log = getLogger(log_name)
    debug = log.debug
    debug(f"Started logger for {log_name}")

    str_dir = str(directory)
    q_choices = [
        "1. Yes.",
        f"2. Yes, and change the config file option to {str_dir} as well.",
        "3. Yes, always create new output directories passed via command line.",
        "4. No.",
        "5. No, and never create new output directories if passed via command line.",
        "6. Let me enter in a new directory.",
        f"7. Use default output directory: {default_data_out}\n",
    ]
    q = f"Intended Output Directory: {str_dir}\n\n"
    q += "\n".join(q_choices) + "\n"
    q += "Since I was unable to find the given output directory, would you like me to create it?: "

    a_nums = []

    for i in range(1, len(q_choices) + 1):
        a_nums.append(i)

    unconfirmed = True

    while unconfirmed is True:
        try:
            a = int(input(q))
        except ValueError as e:
            e_msg = e.__str__()
            msg_match = "invalid literal for int" in e_msg

            if msg_match:
                log.error(
                    "You must provide the number that coincides with what you want to do."
                )
            else:
                log.error("{e_msg}")
                raise

        if a not in a_nums:
            log.warning(f"That is not a valid choice! Please choose from {a_nums}!")
            subprocess.run("clear")
            for i in range(5):
                log.warning("Not a valid choice")
                sleep(0.2)
                sleep(0.2)
        else:
            if a == 1:
                debug(
                    "User expressed a desire to use the directory provided in the command-line argument one time only."
                )
                final_outpath = directory
                unconfirmed = False
                return True
