class NudeManError(Exception):
    def __init__(self, *args, **kwargs):
        """

        Exists as a parent class to all the other exceptions in this module

        Error Code: 100-00

        Args:
            *args ():
            **kwargs ():

        Attributes:
            wiki_page (str): The GitLab wiki page concerning this exception class
        """

        super().__init__(args, kwargs)
        self.wiki_page = (
            "https://gitlab.com/tayjaybabee/nude-man/-/wikis/Error/NudeManError"
        )
        self.message = 'An error of type "NudeManError" has occurred.'
        self.code = 100


class InvalidAppImportError(NudeManError):
    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.wiki_page = "https://gitlab.com/tayjaybabee/nude-man/-/wikis/Error/NudeManError/InvalidAppImportError"
        self.base_msg = "You have attempted to import NudeMan illegally!"
        self.xtra_msg = ""
        if "msg" in kwargs.keys():
            self.xtra_msg = kwargs["msg"]
        elif "message" in kwargs.keys():
            self.xtra_msg = kwargs["message"]

        self.code_str = f"{str(self.code)}-01"
        self.code = 1001
        message = self.message + "\n" + self.base_msg
        self.message += message
        if not self.xtra_msg.replace(" ", "") == "":
            self.message += self.xtra_msg

        self.message += f"Error code: {self.code_str}"


class OutDirNonexistantError(NudeManError):
    def __init__(self):
        """__init__ An exception class indicating that a non-existant output-directory was specified.

        An exception class for use in advising the user that the output directory that was specified in either the command-line arguments or in the configuration file does not exist.
        """
        super().__init__()
        self.message = "The specified output directory does not exist!"


class NoFilesMatchError(NudeManError):
    def __init__(self):
        super().__init__()
        self.message = "No files to move!"
