class APIError(Exception):
    """
    Initialize a new instance of APIError

    This is a base exception for all exceptions of type APIError
    """

    def __init__(self):

        self.api_url = "https://deepai.org"
        """
        URL that leads directly to the third-party site NudeMan queries regarding your photos
        """

        self.message = "An error of type APIError has occurred"
        """
        A message briefly summarizing what problem has occurred

        NOTE:
            In the case of this class, however, this 'message' statement acts as a pointer
            toward filing an issue. This is because the developer does not imagine that
            you'd raise this exception alone without having first raised one of it's heirs.

        """


class ScoringError(APIError):
    def __init__(self, img_name):
        """
        Instantiate a new exception class; ScoringError

        This exception can/will be raised if the API returns an unforeseen (by the developer)
        status code and/or does not contain the score information nude-man needs in order to properly
        sort it.

        Args:
            img_name (str): A string containing the name of the image that failed to return a score
        """
        super().__init__()

        self.message += f"\nAn unknown error has occurred while scoring {img_name}"
        """
        A string that can provide the end-user a bit more context about the exception.
        """


class KeyMissingError(APIError):
    def __init__(self):
        """
        Instantiate a new instance to be raised as an exception.

        This exception will be raised if NudeMan was unable to fetch a valid API key from
        it's configuration file.

        NOTE:
            By default your NudeMan configuration file can be found at
            `/home/$USER/Inspyre-Softworks/run/conf/` with the name `nude-man.conf`

            Be advised, however, that upon first-run of NudeMan or running
            `nude-man configure` the program asks you where you'd like the applications
            data directory to be placed, so if you changed that from the default stated above
            you will always find the following directory structure when looking at a
            non-malformed 'NudeMan' application directory no matter where it's placed:

            -> changeable\ parent\ dir:
              -> Inspyre-Softworks/:
                -> run/:
                  -> conf/:
                    -> nude-man.conf
                  -> pid/:
                -> data/:
                  -> input/:
                  -> output/:

        """
        super().__init__()

        self.message += (
            f"\nThere is no API key from {self.api_url} configured in nude-man.conf."
        )
        self.message += '\nRun "nude-man configure" or edit nude-man.conf'
        """
        A helpful message for if the end-user/developer raises this exception.
        """


class InvalidKeyError(APIError):
    def __init__(self, key):
        """
        Instansiate a new instance of InvalidKeyError

        This exception raises when an invalid API key was provided to the API, according to their status message.

        Args:
            key (str): A copy of the offending key.
        """
        super().__init__()

        self.message += "\nInvalid API Key in specified in configuration file!"
        self.message += f"\nCurrently configured key: {key}"
        """
        A helpful message to be delivered in the event of this exception being raised.
        """
