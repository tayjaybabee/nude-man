"""

A module centering around the argument parser for NudeMan from Inspyre Softworks.

"""
import argparse
import os
from pathlib import Path
from nude_man.app import DEFAULT_DATA_DIR


def parse():
    """
    Parse command-line arguments

    Returns:
        argparse.ArgumentParse: An object containing an object containing command-line-arguments.
    """
    parser = argparse.ArgumentParser(
        "nude-man",
        description="Automatically sort through your pictures and quickly/easily "
        "get them moved to a more suitable location; should help prevent "
        "accidental indecent exposure, but I make no guarantees.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        required=False,
        default=False,
        help="Enable verbose output to the console.",
    )
    parser.add_argument(
        "-r",
        "--recursive",
        action="store_true",
        default=True,
        help="Passing this flag will instruct the program not to recursively scan the source directory.",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        default=False,
        help="Passing this flag will run the program as usual but skips the step at the end where it copies the files it finds.",
    )
    parser.add_argument(
        "-k",
        "--api-key",
        type=str,
        action="store",
        help="The API key that you get when you register for free at https://deepai.org/",
    )
    parser.add_argument(
        "-t",
        "--threshold",
        type=float,
        action="store",
        default=0.75,
        help='Any float value between 0.00 and 1.00. If the program gives a photo a "NSFW Score" '
        "that falls above this number, it will be flagged for sweeping.",
    )
    parser.add_argument(
        "--data-store",
        type=str,
        action="store",
        required=False,
        default=f'{str(Path("~/Inspyre-Softworks/NudeMan").expanduser())}',
        help=f"Provide a directory for NudeMan to store it's data in, if you're unhappy with the default: {DEFAULT_DATA_DIR}",
    )
    parser.add_argument(
        "-s",
        "--source-start",
        type=str,
        action="store",
        default=f'{str(Path("~/Inspyre-Softworks/NudeMan/data/input").expanduser())}',
        help="Indicate the directory the program should start its scan in",
    )
    parser.add_argument(
        "-o",
        "--output-directory",
        type=str,
        action="store",
        default=f'{str(Path("~/Inspyre-Softworks/NudeMan/data/output").expanduser())}',
        help="Indicate where the files we find should be swept away to.",
    )
    subparsers = parser.add_subparsers(dest="subcommand_name")
    conf_util = subparsers.add_parser(
        "configure", help="Run NudeMan's configuration utility"
    )
    conf_util.add_argument(
        "-e",
        "--edit",
        action="store_true",
        default="None",
        required=False,
        help="Load an existing file per '~/.cache/Inspyre-Softworks/NudeMan/run/env.lock'",
    )
    return parser.parse_args()


def dir_path(path):
    """
    Determine if the provided path exists or not

    Provide this function a path or string object that can be converted to a PosixPath object
    to query it's current existence. This is abstracted out for logging purposes.

    Args:
        path (str): A string containing the path you wish to query

    Raises:
        argparse.ArgumentTypeError: This exception will be raised if the path provided is not a valid path

    Returns:
        PosixPath: If the function was able to find our system path it will return that very path to us.
    """
    if os.path.isdir(path):
        return path
    else:
        raise argparse.ArgumentTypeError(f"readable_dir:{path} is not a valid path")
