import os
from pathlib import Path
from configparser import ConfigParser
import argparse
from argparse import ArgumentParser
from datetime import datetime
from nude_man.lib.config.environment.errors import (
    InvalidEnvLockError,
    MissingEnvLockError,
)
from nude_man.lib.errors import InvalidArgumentTypeError
from nude_man.lib.helpers.debug import format_members

from logging import getLogger


class Environment(object):
    def __init__(self, arg_parser: argparse):
        """

        Initialize a new instance of the Environment class.

        The Environment class handles things like early assessment of the file-system, and where to find a
        previously-written config file.

        ToDo:
            - Explain env.lock file in docstrings

        Args:
            arg_parser (ArgumentParser): This should be an ArgumentParser object that has already been initialized
            and fed options/arguments.

        """

        # Set logger up for this classalue for arg_parser is the correct type, else raise an exception

        # Name it
        self.log_name = "NudeMan.lib.config.Environment"

        # Initialize a child-logger
        self.log = getLogger(self.log_name)

        # Given the appropriate log-level is set; announce our name and that our logger is functional
        self.log.debug(f"Started logger for {self.log_name} class.")
        self.log.debug(f"I introduce the following members:")

        # Assess our location in the filesystem
        self.current_filepath = os.path.dirname(os.path.realpath(__file__))

        # Create a filepath that first goes 'up' 4 directories before entering the appropriate directory (
        # $PROG_ROOT/nude_man/conf/env.lock)
        self.conf_dirpath = str(self.current_filepath + str("../" * 4 + "conf"))

        # Narrow down the direct filepath of our lock file:
        self.conf_filepath = self.conf_dirpath + "/env.lock"

        # A placeholder that will also work as a loop enforcer
        self.existing = False

        # Prepare for possible 10107 errors
        try:

            # Check to see if '$PROG_ROOT/nude_man/conf' exists already as a directory.
            if Path(self.conf_dirpath).resolve().exists():

                # Check to see if the conf_filepath points to a file
                if Path(self.conf_filepath).resolve().is_file():

                    # After these checks are passed (to avoid messy exceptions) we load the file into a variable
                    self.env = self.load_file()

                    # We tell the loop enforcer to stop enforcing
                    self.existing = True
                else:

                    # If we can't find the config file, we create a new one, fully editable
                    self.env = self.create(arg_parser)
            else:

                raise MissingEnvLockError()

        except MissingEnvLockError as e:
            self.log.error(e.message)

    def write_file(self):
        """
        Write the env.lock file to disk

        Returns:
            None

        """
        with open(Path(self.conf_filepath).resolve(), "w") as fp:
            self.env.write(fp)

    def load_file(self):
        """
        Load the env.lock file to memory

        Returns:
            ConfigParser

        """
        fp = Path(self.conf_filepath).resolve()
        c_parser = ConfigParser()
        c_parser.read(fp)
        c_parser.sections()
        if not "environment".upper() in c_parser.sections():
            raise InvalidEnvLockError()
        else:
            return c_parser

    def create(self, arg_parser):
        """
        Create a new structure to go inside a new env.lock file for later usage

        Args:
            arg_parser (ArgumentParser): An ArgumentParser object already initialized and parsed.

        Returns:
            ConfigParser: A ConfigParser object that can be written to a file to be read later

        """
        c_parser = ConfigParser()
        env = {
            "ENVIRONMENT": {
                "creation_date": datetime.now(),
                "data_dir": arg_parser.data_store,
            }
        }

        c_parser.read_dict(env)
        print(c_parser)

        self.env = c_parser

        try:
            self.write_file()
        except:
            raise

        return c_parser
