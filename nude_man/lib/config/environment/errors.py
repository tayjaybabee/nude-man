class EnvironmentError(Exception):
    def __init__(self):
        """__init__ An exception to be raised upon the program's discovery of
        an environment error.

        An exception of this type will nearly always have a child type.

        """
        self.message = 'An error of type "EnvironmentError" has occurred!'


class MissingEnvLockError(EnvironmentError):
    def __init__(self):
        super().__init__()
        self.message += '\nWas not able to find environment directory'


class InvalidEnvLockError(EnvironmentError):
    def __init__(self):

        super().__init__()
        self.message += '\nWas able to find env.lock, but it\'s malformed!'
        self.message += "\nIt might be helpful to run NudeMan's configure command again:"
        self.message += '\n    nude-man configure <options>'
        self.message += '\n\nNOTE:\nRunning the above command will wipe your current configuration!'
        self.message += '\n' + str('---->Back it up!<----' * 3)
