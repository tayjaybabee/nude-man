from configparser import ConfigParser

from nude_man.lib.helpers.files import (
    SIZE_UNIT,
    SizeCodex,
    get_size as get_file_size,
    grab_paths as grab_file_paths,
    makedir_confirm,
)
from nude_man.lib.helpers.debug import format_members
from nude_man.lib.errors.api import (
    APIError,
    InvalidKeyError,
    KeyMissingError,
    ScoringError,
)
from nude_man.lib.errors.app import (
    InvalidAppImportError,
    OutDirNonexistantError,
    NudeManError,
    NoFilesMatchError,
)
from nude_man.lib.config.args import parse as parse_args
from nude_man.app import DEFAULT_DATA_DIR

from inspy_logger import start as start_logger
from nude_man.lib.errors.api import InvalidKeyError, KeyMissingError, ScoringError
from nude_man.lib.errors import InvalidArgumentTypeError


from logging import getLogger
import requests
from tqdm import tqdm

__version__ = "0.1.0"

APP_NAME = "NudeMan"

log_name = str(f"{APP_NAME}.Main")
log = getLogger(log_name)
debug = log.debug
debug(f"Imported {__file__}!")


found = []

final_outpath = None


def conclude(args, processed, dest):
    if processed is None:
        raise NoFilesMatchError
    final_outpath = dest
    log_name = "NudeMan.conclude"
    log = getLogger(log_name)
    debug = log.debug

    debug(f'Logger started for "{log_name}"')
    debug(f"Received {len(processed)} files to move to {dest}")

    if args.dry_run:
        log.info(f"I found")
    else:
        for file in tqdm(processed):
            s_path = Path(file["filepath"]).resolve()
            fp = Path(str(dest) + f'/{file["name"]}.{file["ext"]}')
            log.debug(f'Copying {file["name"]} to {fp} from {s_path}')

            try:
                shutil.copy(s_path, fp)
            except NotADirectoryError as e:
                log.warning(
                    f"Unable to find directory I need to write these pictures into: {dest}"
                )
                confirm = makedir_confirm(dest)
                if confirm:
                    os.makedirs(dest)
                    conclude(args, processed, dest)
                else:
                    raise e

            except FileNotFoundError as e:
                log.warning(
                    f"Unable to find directory I need to write these pictures into: {dest}"
                )
                confirm = makedir_confirm(dest)
                if confirm:
                    os.makedirs(dest)
                    conclude(args, processed, dest)
                else:
                    raise e


def score(pic, key):
    res = requests.post(
        "https://api.deepai.org/api/nsfw-detector",
        files={"image": open(pic["filepath"], "rb"),},
        headers={"api-key": f"{key}"},
    )

    data = res.json()
    out_name = pic["name"]

    if "err" in data.keys() or not "output" in data.keys():
        raise ScoringError(out_name)

    if "status" in data.keys():
        if "Please pass a valid Api-Key" in data["status"]:
            if key:
                raise InvalidKeyError(key)
            else:
                raise KeyMissingError()

    data_str = str(data)
    log.debug(data_str)
    score = int(data["output"]["nsfw_score"] * 100)
    pic["nsfw_score"] = score
    log.debug(f"{out_name}: {score}%")

    log.debug(f"Returning the following to caller: {pic}")

    return pic


def score_list(pic_list: list, config: ConfigParser):
    """

    This function takes a list of dictionaries and a ConfigParser object and - while checking each file represented in the list using the `score` function - iterates until the end of the list

    Arguments:
        pic_list: A list of Dictionary objects

    """
    log_name = "NudeMan.score_checker"

    if not isinstance(pic_list, list):
        raise ValueError()

    if not isinstance(config, ConfigParser):
        raise ValueError()

    log = getLogger(log_name)
    debug = log.debug
    debug(f"Logger started for {log_name}!")
    over_threshold = []
    pic_num = len(pic_list)

    threshold = config["SETTINGS"]["threshold"]
    debug(f"Threshold set at {float(threshold) * 100}%")

    debug(f"Iterating over {pic_num} files.. ")

    for _file in tqdm(pic_list):
        try:
            pic = score(_file, config["SETTINGS"]["api-key"])
        except ScoringError as e:
            log.error(e.message)
            return

        if int(pic["nsfw_score"]) >= int((float(threshold) * 100)):
            log.debug(f'{pic["name"]} determined to be over threshold!')
            over_threshold.append(pic)
            log.debug("Added to queue.")

    num_nsfw = len(over_threshold)
    stats = str(str(num_nsfw) + f"/{pic_num}")
    log.info(
        f"{stats} pictures determined to have a NSFW score over {float(threshold) * 100}%"
    )

    return over_threshold


debug(f"I introduce the following members: {format_members(dir(), False)}")
