from logging import getLogger


from inspy_logger import start as start_logger
from nude_man.lib.errors.app import InvalidAppImportError


def run():
    from nude_man.lib.config.args import parse as parse_args

    # Parse arguments, and get resulting object.
    args = parse_args()

    # Prepare our root logger by first designating a name
    root_log_name = "NudeMan"

    # Then pass that name, and the resulting bool from evaluating args.verbose to inspy_logger.start
    root_log = start_logger(root_log_name, args.verbose)

    # Then name the actual logger for this function, and use logging.getLogger to create a child to our root
    main_log_name = root_log_name + ".main"
    main_log = getLogger(main_log_name)

    # Shorten the often-called 'main_log.debug' to just 'debug'
    debug = main_log.debug

    # Announce some debugging stuff
    debug(f"Logger started for {main_log_name}")
    debug(f"nude-man was started with these options: {args}")


if __name__ == "__main__":
    raise InvalidAppImportError()
else:
    run()
