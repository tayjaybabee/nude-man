## Developer Task [Version Control System]
*Template for developer tasks that focus on making changes/corrections to files for the VCS but not actual code.*

### Needed Correction:
*Summarize the needed change/correction here*

### Cause for Correction:
*Why should this be corrected?*

### File(s) Affected:
*Here you can enter a list of files are affected by this change.*

 - nude_man.lib.config

### Task Checklist:
*What do you need to do to complete this task?*
 - [ ] Make correction
 - [ ] Etc
   - [ ] Etc

### Additional Comments:

**Developer Task Notes**

> DATE/TIME - INITIALS
> Comment


/label ~vcs ~"Developer Task::NCA"
/assign @tayjaybabee
/confidential
