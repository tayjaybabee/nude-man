## Developer Task [General Corrections-Non Code Affecting]
*Template for issues that are Developer Tasks that require changes to things like docstrings, doc pages, in-line comments. (For type correction etc...) that do NOT affect any of the code of the project.*

### Needed Correction:
*What needs to be corrected?*

### Cause for Correction:
*Why should this be corrected?*

### File(s) Affected:
*Here you can enter a list of files in dot-form that are affected by this change.*

 - nude_man.lib.config

### Task Checklist:
*What do you need to do to complete this task?*
 - [ ] Make correction
 - [ ] Etc
   - [ ] Etc

### Additional Comments:
*Any additional information that might help troubleshoot this issue?*


/label ~"Developer Task::NCA", ~Status::Unaddressed
/assign @tayjaybabee
/confidential
